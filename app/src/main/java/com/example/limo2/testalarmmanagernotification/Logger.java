package com.example.limo2.testalarmmanagernotification;

import android.util.Log;

/**
 * Created by Ahmed on 11/15/2016.
 */

public class Logger {
    private static final String TAG = "Limo";

    public static void forDebug(String dbgMsg){
        Log.d(TAG,dbgMsg);
    }

    public static void forDebug(String tag,String dbgMsg){
        Log.d(tag,dbgMsg);
    }
    public static void forInfo(String infoMsg){
        Log.i(TAG,infoMsg);
    }
    public static void forError(String errMsg){
        Log.e(TAG,errMsg);
    }
}
