package com.example.limo2.testalarmmanagernotification;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.limo2.testalarmmanagernotification.services.AlarmUtil;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button start=findViewById(R.id.startAlarm);
        Button cancel=findViewById(R.id.cancelAlarm);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlarmUtil(MainActivity.this).setEarningAlarm();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlarmUtil(MainActivity.this).cancelEarningAlarm();
            }
        });

    }
}
