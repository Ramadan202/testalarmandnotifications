package com.example.limo2.testalarmmanagernotification.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.example.limo2.testalarmmanagernotification.Logger;

import java.util.Calendar;

/**
 * Created by Ahmed on 12/6/2016.
 */

public class AlarmUtil {

    private AlarmManager manager;
    private Context context;

    public AlarmUtil(Context context) {
        this.context = context;
        manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    public void setEarningAlarm() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 13);
        calendar.set(Calendar.MINUTE, 31);
        calendar.set(Calendar.SECOND, 0);

        /*manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                8000 ,getRecieverPendingIntent());*/
        manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, getRecieverPendingIntent());
        Logger.forDebug("AlarmManager:alarmSet");
    }

    private PendingIntent getRecieverPendingIntent() {
        Intent intent = new Intent(context, TodayEarningReceiver.class);
        return PendingIntent.getBroadcast(context, 0,intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void cancelEarningAlarm(){
        manager.cancel(getRecieverPendingIntent());
        Logger.forDebug("AlarmManager:cancelEarningAlarm");
    }

}
