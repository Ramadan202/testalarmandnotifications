package com.example.limo2.testalarmmanagernotification.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.limo2.testalarmmanagernotification.MainActivity;
import com.example.limo2.testalarmmanagernotification.NotificationUtils;
import com.example.limo2.testalarmmanagernotification.R;

public class TodayEarningReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationUtils notificationUtils = new NotificationUtils(context);
        Intent earningIntent = new Intent(context, MainActivity.class);
        notificationUtils.showNotificationMessage(context.getString(R.string.total_earning_for_today)
                ,earningIntent,NotificationUtils.EARNING_ID);
    }
}